# winres

一个可以给rust程序添加图标或元数据之类的库,只支持Windows。

[文档](https://docs.rs/winres/*/winres/)

## 工具

在使用之前，你需要安装这些
 - [Windows SDK] 中的 `rc.exe`
 - [minGW64] `windres.exe` 和 `ar.exe` 
 
[Windows SDK]: https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk
[minGW64]: http://mingw-w64.org

如果你使用 MSVC ABI 你需要下载 Windows SDK，如果你使用 GNU ABI 你需要使用 minGW64


## 使用教程

首先，你需要在你的 `Cargo.toml` 文件中添加 `build`。

```toml
[package]
#...
build = "build.rs"

[build-dependencies]
winres = "0.1"
```

然后，你需要创建一个 `build.rs` ,代码如下。

```rust
// build.rs

extern crate winres;

fn main() {
    let mut res = winres::WindowsResource::new();
    res.set_icon("test.ico");
    res.compile().unwrap();
}
```

`test.ico` 文件应该和 `build.rs` 放在一起。


## 其他

你可以在 `Cargo.toml` 中的 `package.metadata.winres` 里写上程序的详细信息。

```toml
[package.metadata.winres]
ProductName = "产品名称"
FileDescription = "文件说明"
LegalCopyright = "版权"
ProductVersion = "产品版本"
OriginalFilename = "原始文件名"
#...
```

这些可以在 [MSDN](https://msdn.microsoft.com/en-us/library/windows/desktop/aa381058.aspx) 中找到。




下边是原作者的留言。

## About this project

I've written this crate chiefly for my personal projects and although I've tested it
on my personal computers I have no idea if the behaviour is the same everywhere.

To be brief, I'm very much reliant on your bug reports and feature suggestions
to make this crate better.
